user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;
worker_rlimit_nofile 204800;

events {
  use epoll;
  worker_connections 102400;
  # multi_accept on;
}

http {
  include /etc/nginx/mime.types;
  include /etc/nginx/http-access.conf;
  default_type  application/octet-stream;

  sendfile on;

  server_tokens on;

  types_hash_max_size 1024;
  types_hash_bucket_size 512;

  server_names_hash_bucket_size 64;
  server_names_hash_max_size 512;

  keepalive_timeout  65;
  tcp_nodelay        on;

  gzip              on;
  gzip_vary         on;
  gzip_comp_level   6;
  gzip_buffers      16 8k;
  gzip_http_version 1.1;
  gzip_proxied      any;
  gzip_min_length   500;
  gzip_disable      "MSIE [1-6]\.";
  gzip_types        text/plain text/xml text/css
                    text/comma-separated-values
                    text/javascript
                    application/json
                    application/xml
                    application/x-javascript
                    application/javascript
                    application/atom+xml;

  proxy_redirect          off;
  proxy_connect_timeout   90;
  proxy_send_timeout      90;
  proxy_read_timeout      90;
  proxy_buffers           32 4k;
  proxy_buffer_size       8k;
  proxy_set_header         Host $http_host;
  proxy_set_header         X-Real-IP $remote_addr;
  proxy_set_header         X-Forward-For $proxy_add_x_forwarded_for;
  # when redirecting to https:
  # proxy_set_header         X-Forwarded-Proto https;
  proxy_set_header         X-Forwarded-Host $http_host;
  proxy_headers_hash_bucket_size 64;

  # List of application servers
  upstream app_servers {
    server odoo:8069;
  }

  # Configuration for the server
  server {
    limit_conn one 20;
    limit_rate_after 10m;
    limit_rate 100k;

    listen 80 default;

    client_max_body_size 1G;

    add_header              Strict-Transport-Security "max-age=31536000";

    location / {
      proxy_pass http://odoo:8069;
      proxy_read_timeout    6h;
      proxy_connect_timeout 5s;
      proxy_redirect        off;
      #proxy_redirect        http://$host/ https://$host:$server_port/;

      add_header X-Static no;
      add_header X-Cache $upstream_cache_status;
      proxy_buffer_size 64k;
      proxy_buffering off;
      proxy_buffers 4 64k;
      proxy_busy_buffers_size 64k;
      proxy_intercept_errors on;

    }
    location /longpolling/ { proxy_pass http://odoo:8069/longpolling/; 
    proxy_redirect        off;}


    location ~* /web/static/ {
        proxy_cache_valid 200 60m;
        proxy_buffering on;
        expires 864000;
        proxy_pass http://odoo:8069;
    }

  }
}